## Android Lobi 8.19.0(266)

- アカウント切り替えができる最後のバージョンです
- [https://www.dropbox.com/s/nz0cwuo67l53nfk/com.kayac.nakamap-266.apk](https://www.dropbox.com/s/nz0cwuo67l53nfk/com.kayac.nakamap-266.apk)

- 自動更新がかからないようデバッグキーで署名し直したバージョンです
    - ダウンロード一覧から直接開けない場合はアストロファイルマネージャ等でインストールできます
- [https://www.dropbox.com/s/u9t9jgbom0rdnhe/com.kayac.nakamap-266-resigned.apk](https://www.dropbox.com/s/u9t9jgbom0rdnhe/com.kayac.nakamap-266-resigned.apk)
