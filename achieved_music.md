## 取得楽曲

### 凡例
- <font color="teal">緑鍵</font>
- <font color="orange">黄鍵</font>
- <font color="vermilion">祭曲をラ祭以外の方法で入手</font>

|曲数|日時|取得楽曲|
|:--|:--|:--|
|10|1 / 3|Wanna Be Your Special / Touch Of Gold / 雨の音が虹を呼ぶ / DIAMOND SKIN / Wasteland<br>When You Call My Name / キミとMUSIC / Aqualight / Wanyo Wanyo / Lotus Love|
|13|1 / 5|Loveless / Landing on the moon / Jahacid|
|16|1 / 6|Freak With Me / ホントのワタシ / Dirty Mouth|
|18|1 / 8|Hi / Binary Overdrive|
|19|1 / 9|Anomie|
|20|1 / 12|Otherside|
|21|1 / 13|What is Wrong With The World|
|23|1 / 15|Break down / エアリアル|
|24|1 / 20|DAZZLING♡SEASON|
|25|1 / 22|<font color="teal">Eternally</font>|
|26|1 / 23|Giant Killing|
|28|1 / 24|Azitate / <font color="orange">Cross + Angel</font>|
|29|1 / 26|<font color="teal">everKrack</font>|
|30|1 / 28|<font color="teal">Winter, again</font>|
|31|2 / 7|**未確認XX生命体**|
|32|2 / 8|**NEXT FRONTIER**|
|33|2 / 11|**The Epic**|
|34|2 / 14|How True Is Your Love|
|36|2 / 15|nightmares / default affinity|
|37|2 / 19|<font color="teal">JUSTICE from GUILTY</font>|
|38|2 / 20|<font color="teal">Re:Milky way</font>|
|41|2 / 21|<font color="teal">エール</font> / <font color="teal">誘惑</font> / <font color="teal">オーバーラップ</font>|
|42|2 / 22|<font color="teal">Nu Heat</font>|
|44|3 / 10|BASS ANTICS / Doki Blaster|
|45|3 / 12|BRAND NEW|
|46|3 / 22|<font color="teal">最後の１ページ</font>|
|48|4 / 8|boule de berlin / cloudstepping|
|50|4 / 9|残響のアカーシャ / **Impact**|
|51|4 / 11|the Last Missile Man|
|53|4 / 23|光年 / Human Nature|
|55|5 / 5|<font color="teal">Azitate (Prologue Edition)</font> / <font color="orange">Touch Of Gold (Bongo Mango Remix)</font>|
|56|5 / 6|<font color="teal">HEADPHONE PARTY</font>|
|58|5 / 10|イルミナレガロ / Deep Outside|
|60|5 / 23|Harmony / Flame Up|
|61|5 / 27|<font color="teal">ツカイステ・デッドワールド</font>|
|62|5 / 28|<font color="orange">最終回STORY</font>|
|63|5 / 29|<font color="teal">Landing on the moon (Inst)</font>|
|64|6 / 1|<font color="orange">Into the Cave</font>|
|65|6 / 6|<font color="orange">Wonderful</font>|
|67|6 / 16|Indigo Isle / Free|
|78|6 / 21|killy killy JOKER / 海色 / 白鳥の湖 / トルコ行進曲 / イジメダメゼッタイ<br>Hopes Bright / いいね！ / Flowerwall / ギミチョコ / サバイバル<br>ヘドバンギャー|
|80|6 / 24|<font color="teal">Sunny day</font> / <font color="teal">Vespero</font>|
|81|6 / 28|<font color="orange">Anomie (Axiom Style)</font>|
|82|6 / 29|<font color="orange">NEXT FRONTIER -TRUE RISE-</font>|
|83|7 / 4|<font color="orange">Murasame</font>|
|85|7 / 5|ソンザイキョウドウタイ / 妄想全開|
|86|7 / 8|tokyo|
|88|7 / 21|Bi-Zon Zon Zombi (More) / 光年 Remix|
|91|7 / 22|<font color="orange">Reseed</font> / <font color="teal">メギツネ</font> / <font color="teal">Dreaming Girl</font>|
|97|7 / 23|<font color="teal">Make It Fresh</font> / <font color="teal">逆転裁判</font> / <font color="orange">Raise Your Handz</font> / <font color="orange">チルドレン・オートマトン</font> / <font color="orange">Designed World</font><br><font color="teal">What is Wrong With The World (Cross)</font>|
|98|7 / 24|<font color="orange">Sundrop (Remix)</font>|
|101|7 / 25|<font color="teal">RIDE ON NOW</font> / <font color="orange">ロマンシングゲーム</font> / <font color="orange">ロックマン</font>|
|103|7 / 26|<font color="teal">MERCURY</font> / <font color="teal">some day (instrumental)</font>|
|105|7 / 27|<font color="teal">Voice Of House</font> / **Z\[i\]**|
|106|7 / 29|<font color="orange">戦国BASARA</font>|
|107|7 / 30|<font color="orange">Virtual Reality Controller</font>|
|109|7 / 31|<font color="orange">ピコラセテ (Inst)</font> / <font color="orange">Sapphire</font>|
|110|8 / 2|Reuniverse|
|111|8 / 5|VOHリミ|
|113|8 / 17|Always Thinkg Of You / Machine|
|114|8 / 18|<font color="orange">Timeless encode</font>|
|115|8 / 20|<font color="teal">Moonrise</font>|
|116|8 / 21|<font color="orange">OKOKUNIKOD</font>|
|117|8 / 23|<font color="orange">ホコロビシロガールズ</font>|
|118|8 / 28|Wanna Be Your Special (Remix)|
|121|9 / 10|ストレンジ・ディーヴァ / Don't Walk Away / All U Need|
|132|9 / 13|シュガーソングとビターステップ / Inner Urge / D.O.B. / ViViD / unfinished<br>Break a spell / メモリーズ / WE GO / X-encounter / Light My Fire<br>show|
|140|9 / 16|<font color="orange">電脳少女</font> / <font color="orange">I.D.</font> / <font color="orange">Sundrop</font> / <font color="orange">some day see you again</font> / <font color="orange">甘い言葉</font><br><font color="orange">Driving story</font> / <font color="orange">覚醒awake</font> / <font color="orange">NIGHT FEELIN</font>|
|143|9 / 17|<font color="orange">大殺界</font> / <font color="orange">現実幻覚スピードスター</font> / <font color="orange">サマ★ラブ</font> / <font color="orange"></font>|
|145|9 / 18|<font color="orange">Addicted</font> / <font color="orange">Break Your World</font>|
|150|9 / 19|<font color="orange">Aqualight (Remix)</font> / <font color="orange">魔界村</font> / <font color="orange">ピコラセテ</font> / <font color="orange">Reanimation</font> / <font color="orange">Omniverse</font>|
|153|9 / 20|<font color="orange">CrossShooter</font> / <font color="orange">Rebellion</font> / <font color="orange">Underworld</font>|
|154|9 / 21|<font color="orange">SUNDAY リベンジ</font>|
|155|9 / 22|<font color="orange">DREAMIN' OF YOU</font>|
|157|10 / 8|Assign / Flame Up (Remix)|
|159|10 / 19|FUNKYBABY EVOLUTION / SuperLuminalGirl Rebirth|
|160|10 / 24|<font color="orange">Bi-Zon Zon Zombi</font>|
|162|11 / 10|Burning Inside / Resonance|
|163|11 / 12|BUZZ Ketos|
|164|11 / 14|**DYNAMITE SENSATION**|
|165|11 / 19|<font color="orange">Break down (2nd)</font>|
|166|11 / 20|<font color="orange">ASTAROTH</font>|
|167|11 / 21|<font color="orange">Another Chance</font>|
|168|11 / 23|<font color="orange">Hisui</font>|
|169|11 / 24|<font color="orange">Designed World (Remix)</font>|
|170|11 / 28|<font color="orange">Crystal Ribbon</font>|
|172|11 / 29|<font color="orange">Soda Machine</font> / <font color="orange">Grand Arc (Club Remix)</font>|
|174|12 / 7|Interstellar Plazma / Sangeyasya|
|175|12 / 21|フォルテシモ|
|177|12 / 25|Metro Night / ULTRiX|
|178|1 / 13|Filament Flow|
|179|1 / 14|The Epic Intro|
|180|1 / 23|<font color="orange">Dazzling Darwin</font>|
|181|1 / 24|<font color="orange">EverWhite</font>|
|182|1 / 26|<font color="orange">Isinglass</font>|
|183|1 / 27|<font color="orange">GARNET HOWL</font>|
|185|1 / 28|<font color="orange">starting station</font> / <font color="orange">SPLASH</font>|
|186|2 / 16|Faraway|
|187|2 / 26|<font color="orange">Perfectionism</font>|
|188|3 / 14|only L|
|189|3 / 29|<font color="orange">Take It Back</font>|
|191|4 / 26|ChipNotch / ネコふんじゃった|
|192|4 / 28|<font color="orange">STARDUST</font>|
|193|4 / 29|<font color="orange">Over The Blue</font>|
|194|5 / 8|<font color="orange">Frgmnts</font>|
|196|5 / 19|Adverse / Planet Calling|
|200|5 / 25|流星 / South wind / BladeMaster / <font color="orange">たゆたい</font>|
|201|5 / 26|<font color="orange">IONIZATION</font>|
|203|6 / 13|MilkyWayTrip / Cash|
|207|6 / 26|<font color="orange">Epiphany</font> / <font color="vermilion">Grand Arc</font> / Ariel / Phoenix|
|208|6 / 30|<font color="orange">KING STUN</font>|
|212|7 / 16|HDM / Pyromania / 最終列車 / 星達のメロディ|
|217|7 / 27|<font color="orange">YUKARI</font> / <font color="orange">Days</font> / <font color="orange">勇敢</font> / <font color="orange">Code Paradiso</font> / <font color="orange">FarthestEnd</font>|
|218|7 / 28|<font color="orange">Curse of Doll</font>|
|219|8 / 5|<font color="orange">Into the Cave Another</font>|
|220|8 / 8|<font color="orange">Come Around</font>|
|221|8 / 10|Silbury Sign|
|223|8 / 21|カウンターストップ / 未来へのプレリュード|

### 月別獲得数
- 30,12,4,7,10,19,**27**,9,**37**,5,12,5
- 8,2,2,5,8,7,10
