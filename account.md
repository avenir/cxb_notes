## 名前
- いろいろ試したけどユーザー名が取られてたんですよね
- Lobiの表示名が変更できなくなっていなければ、目薬記念に`C[i]el`あたりに変更していたかもしれないですね。

## CxB

- ルウ(39498830)
    - 作ってから名前変更できないのを思い出しお蔵入りになってしまった感じ
    - なおソーシャルゲーム等は(ここで途切れている)

## Lobiアカウント

- [https://lobi.co/invite/mVFs7](https://lobi.co/invite/mVFs7)

## CxBRank

- [http://cxbrank.maplia.jp/view/00652](http://cxbrank.maplia.jp/view/00652)
- [https://revrank.maplia.jp/sunrise/view/00057](https://revrank.maplia.jp/sunrise/view/00057)
