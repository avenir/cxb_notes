(画像が手元に残っていないかもなので、あとで準備したりしなかったりするかも)

## 高速アカウント切り替え

- iOSのアプリサンドボックス領域はiOS6.0以上8.3未満の間でしか操作できない。
- しかし、CxBのセーブデータ領域はファイル共有領域となっているため、例外的にiOS9以降でも問題なくアクセスできる。
- [iFunBox](http://www.i-funbox.com/)などを用いて、SaveData_Crossファイルを削除することにより、追加データを再ダウンロードすることなしにアカウントを切り替えることが出来る。
- この方法は1.14現在 **脱獄不要** で行うことが出来る。

## 母艦フリー

- iFile等を使えば、PCなしでSaveData_Crossを直接削除することが可能である。 *ただし、脱獄が条件となる。*
- ただし、アプリのディレクトリに行き着くのが比較的困難であるため、[applink](https://gist.github.com/cielavenir/089ae47301458db8e2e04a67f72132b3)等でsymlinkを張ることをおすすめする。
